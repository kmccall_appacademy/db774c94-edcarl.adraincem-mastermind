class Code
  PEGS = {
    "r" => "Red",
    "g" => "Green",
    "b" => "Blue",
    "y" => "Yellow",
    "o" => "Orange",
    "p" => "Purple"
  }

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(code)
    code_mod = code.downcase.chars
    if code_mod.all? { |clr| PEGS.include?(clr.downcase) }
      Code.new(code_mod)
    else
      raise "ERROR - invalid colors"
    end
  end

  def self.random
    pegs = Array.new(4).map { PEGS.keys.sample }
    Code.new(pegs)
  end

  def [](index)
    pegs[index]
  end

  def exact_matches(code)
    matches = 0
    0.upto(3) { |idx| matches += 1 if pegs[idx] == code[idx] }
    matches
  end

  def near_matches(code)
    near_hash = {}
    code.pegs.each_with_index do |ch, idx|
      if pegs.include?(ch) && pegs[idx] != code.pegs[idx]
        near_hash[ch] = true
      end
    end
    near_hash.count
  end

  def ==(input)
    return false unless input.is_a?(Code) && exact_matches(input) == 4
    true
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = nil)
    if secret_code.nil?
      @secret_code = Code.random
    else
      @secret_code = secret_code
    end
  end

  def play
    turns = 10
    while turns > 0
      puts "FOR TESTING - SECRET CODE #{secret_code.pegs}"
      code = get_guess
      display_matches(code)
      break if code == secret_code
      turns -= 1
    end

    if turns == 0
      puts "!!! You lose. You ran out of turns. !!!"
    else
      puts "***~~~ You broke the code! ~~~***"
    end
  end

  def get_guess
    puts "Guess the 4 colors to break the code."
    input = $stdin.gets.chomp
    Code.parse(input)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end

end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play
end
